<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersIndexRequest;
use App\Http\Requests\UsersStoreRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Models\User;

//use Doctrine\DBAL\Schema\View;
use Carbon\Carbon;

//use Dotenv\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Faker\Calculator\Iban;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\View\View;
use PHPUnit\TextUI\Configuration\Constant;
use function PHPUnit\Framework\assertNotEmpty;

class UserController extends Controller
{
    protected const INCLUDE_YEAR = 1;

    public function index(UsersIndexRequest $request): View
    {
        $data = $request->validated();
        $searchNameOrEmail = '';
        $searchYearFrom = '';
        $searchYearTo = '';
        $users = User::query();
        if (isset($data['searchNameOrEmail'])) {
            $searchNameOrEmail = $data['searchNameOrEmail'];
            $users->where('name', 'ilike', "%$searchNameOrEmail%")
                ->orWhere('email', 'ilike', "%$searchNameOrEmail%");
        }
        if (isset($data['searchYearFrom'])) {
            $searchYearFrom = $data['searchYearFrom'];
            $ageFromInDbFormat = Carbon::now()->subYear($searchYearFrom)->format('Y-m-d');
            $users->whereDate('birthday', '<=', $ageFromInDbFormat);
        }
        if (isset($data['searchYearTo'])) {
            $searchYearTo = $data['searchYearTo'];
            $ageToInDbFormat = Carbon::now()->subYear($searchYearTo + self::INCLUDE_YEAR)->format('Y-m-d');
            $users->whereDate('birthday', '>', $ageToInDbFormat);
        }
        $users = $users->paginate(10);
        return view('users', [
            'users' => $users,
            'searchNameOrEmail' => $searchNameOrEmail,
            'searchYearFrom' => $searchYearFrom,
            'searchYearTo' => $searchYearTo,
        ]);
    }

    public function create(): View
    {
        return view('users.create');
    }

    public function store(UsersStoreRequest $storeRequest)
    {
        $newUser = $storeRequest->validated();
        $user = new User();
        $user->name = $newUser['newUsername'];
        $user->email = $newUser['newEmail'];
        $user->password = Hash::make($newUser['newPassword']);
        $user->birthday = $newUser['newBirthday'];
        $user->save();
        return redirect('/users');
    }

    public function edit(User $user): View
    {
        return view('users.edit',
            [
                'user' => $user,
            ]
        );
    }

    public function update(UsersUpdateRequest $request, User $user)
    {
        $data = $request->validated();
        $user->update([
            'name' => $data['name'],
            'birthday' => $data['birthday'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect('/users');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect('/users');
    }
}
