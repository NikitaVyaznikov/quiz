<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersIndexRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'searchNameOrEmail' => 'string|nullable',
            'searchYearFrom' => 'integer|min:0|max:150|nullable',
            'searchYearTo' => 'integer|min:0|max:150|nullable',
        ];
    }
}
