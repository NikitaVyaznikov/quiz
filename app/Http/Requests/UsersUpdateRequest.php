<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UsersUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(User $user): array
    {
          return [
            'name' => 'string|max:255|required',
            'email' => 'prohibited',
            'birthday' => 'required|date|after:1900-01-01|before:tomorrow',
            'password' => 'string|min:8|required|',
        ];
    }
}
