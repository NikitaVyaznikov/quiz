<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersStoreRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'newUsername' => 'string|max:255|required',
            'newEmail' => 'email:rfc,dns|required|unique:users,email',
            'newBirthday' => 'required|date|after:1900-01-01|before:tomorrow',
            'newPassword' => 'string|min:8|required',
        ];
    }
}
