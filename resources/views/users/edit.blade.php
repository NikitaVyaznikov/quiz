@extends('adminlte::page')
@section('content')
<form class="row g-3" method="POST" action="{{route('users.update', $user->id)}}">
    @csrf
    @method ('PATCH')
    <div class="col-md-4">
        <label class="form-label">Имя пользователя</label>
        <input type="text" class="form-control" name="name" value="{{$user->name}}" required>
        <div class="valid-feedback">
            {{ __('Здесь будет текст') }}
        </div>
    </div>
    <div class="col-md-4">
        <label class="form-label">Почта</label>
        <div class="input-group has-validation">
            <span class="input-group-text">@</span>
            <input type="email" class="form-control" name="email"
                   aria-describedby="inputGroupPrepend3 validationServerUsernameFeedback" value="{{$user->email}}" disabled>
            <div class="invalid-feedback">
                {{ __('Здесь тоже') }}
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <label class="form-label">Дата рождения</label>
        <input type="" class="form-control" name="birthday" value="{{$user->birthday->format('d.m.Y')}}" required>
        <div class="valid-feedback">
            {{ __('И здесь') }}
        </div>
    </div>

    <div class="col-md-4">
        <label class="form-label">Пароль</label>
        <input type="password" class="form-control" name="password"
               aria-describedby="validationServer05Feedback" value="{{$user->password}}" required>
        <div class="invalid-feedback">
            {{ __('Тут тоже') }}
        </div>
    </div>

    <div class="col-12 p-2">
        <button name="save" class="btn btn-success" type="submit">Обновить</button>
    </div>
</form>
@endsection
