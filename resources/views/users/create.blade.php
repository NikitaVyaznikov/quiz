@extends('adminlte::page')
@section('content')
<form class="row g-3" method="POST" action="{{route('users.store')}}">
    @csrf
    <div class="col-md-4">
        <label class="form-label">Имя пользователя</label>
        <input type="text" class="form-control" name="newUsername" value="" required>
        <div class="valid-feedback">
            {{ __('Здесь будет текст') }}
        </div>
    </div>
    <div class="col-md-4">
        <label class="form-label">Почта</label>
        <div class="input-group has-validation">
            <span class="input-group-text">@</span>
            <input type="email" class="form-control" name="newEmail"
                   aria-describedby="inputGroupPrepend3 validationServerUsernameFeedback" value="" required>
            <div class="invalid-feedback">
                {{ __('Здесь тоже') }}
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <label class="form-label">Дата рождения</label>
        <input type="date" class="form-control" name="newBirthday" value="" required>
        <div class="valid-feedback">
            {{ __('И здесь') }}
        </div>
    </div>

    <div class="col-md-4">
        <label class="form-label">Пароль</label>
        <input type="password" class="form-control" name="newPassword"
               aria-describedby="validationServer05Feedback" value="" required>
        <div class="invalid-feedback">
            {{ __('Тут тоже') }}
        </div>
    </div>

    <div class="col-12 p-2">
        <button name="save" class="btn btn-success" type="submit">Сохранить</button>
    </div>
</form>
@endsection
