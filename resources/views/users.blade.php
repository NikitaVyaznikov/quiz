@extends('adminlte::page')
@section('content')
<div class="d-grid gap-2 d-md-flex justify-content-md-start p-1">
    <a href="{{url ('/users/create')}}" class="col-3" type="button">
        <button class="btn btn-secondary" type="button">Создать нового пользователя</button>
    </a>
</div>
<form action="{{route('users.index')}}" method="get">
    <div class="row align-items-start p-1">
        <div class="col-6">
            <input type="text" class="form-control" name="searchNameOrEmail" placeholder="Поиск по имени и почте"
                   value="{{$searchNameOrEmail}}">
        </div>
        <div class="col-2">
            <input type="number" class="form-control" name="searchYearFrom" placeholder="Возраст от"
                   value="{{$searchYearFrom}}">
        </div>
        <div class="col-2">
            <input type="number" class="form-control" name="searchYearTo" placeholder="Возраст до"
                   value="{{$searchYearTo}}">
        </div>
        <div class="col-2">
            <button type="submit" name="filter" class="btn btn-success">Поиск</button>
        </div>
    </div>
</form>
<table class="table">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">Имя</th>
        <th scope="col">Почта</th>
        <th scope="col">Возраст</th>
        <th scope="col">Действие</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
    <tr>
        <th scope="row">{{$user->id}}</th>
        <td> {{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->birthday->age}}</td>
        <td>
            <div class="btn-group" role="group" aria-label="Basic example">
                <a href="{{route ('users.edit', $user->id)}}">
                    <button type="submit" class="btn btn-dark">Редактировать</button>
                </a>
                <form action="{{route ('users.destroy', $user->id)}}" method="POST">
                    @csrf
                    @method ('DELETE')
                    <button type="submit" class="btn btn-danger">Удалить</button>
                </form>
            </div>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
<div class="mt-3">
    {{ $users->links() }}
</div>
@endsection
